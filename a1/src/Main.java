import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

//        System.out.println("Hello world!");

        String[] fruits = new String[5];
        fruits[0] = "apple";
        fruits[1] = "avocado";
        fruits[2] = "banana";
        fruits[3] = "kiwi";
        fruits[4] = "orange";

        System.out.println("Fruits in stock: " + Arrays.toString(fruits));
        System.out.println("Which fruit would you like to get the index of?");

        Scanner searchIndex = new Scanner(System.in);
        String searchFruit = searchIndex.nextLine();
        int index = Arrays.binarySearch(fruits,searchFruit);

        System.out.println("The index of " + searchFruit + " is: " + index);

        ArrayList<String> friendNames = new ArrayList<>(Arrays.asList("John","Jane","Chloe","Zoey"));

        System.out.println("My friends are: " + friendNames);

        HashMap<String,Integer> inventory = new HashMap<>();
        inventory.put("toothpaste",15);
        inventory.put("toothbrush",20);
        inventory.put("soap",12);

        System.out.println("Our current inventory consists of: ");
        System.out.println(inventory);
    }
}